"""Top-level file. Used to instantiate Flask server.
"""

import os
from flask import Flask
from flask_cors import CORS
from flask_sslify import SSLify

from server.routes import core as core_routes

app = Flask(__name__)
CORS(app)

if os.getenv('CAPITAL_ONE_HACKATHON_APPLICATION_ENVIRONMENT', 'DEVELOPMENT') == 'PRODUCTION':
    SSLify(app)

core_routes.generate_V1_api_from_flask_app(app)

if __name__=='__main__':
    app.run(debug=True)
