"""Classes, methods, and variables for Account.
"""

from flask_restful import Resource

from server.database.models import account as db_account

class Account(Resource):
    """Route model for an Account.
    """
    def get(self, account_id):
        """GET on /account/<string:account_id>.

        Args:
            account_id (str): ID of the account. 'int' cast as 'str'.
        """
        return db_account.Account().find_by_id(int(account_id))


def add_account_resources(api):
    """Adds account resources to the API instance.

    Args:
        api (flask_restful.Api): Flask-RESTful API instance.
    """
    api.add_resource(Account, '/v1/account/<string:account_id>')
