"""Classes, methods, and variables to support Base.
"""

from flask_restful import Resource

class Base(Resource):
    """Route model for a Base.
    """
    def get(self):
        """GET on />.
        """
        # TODO: ACTUALLY DO THIS
        return {'hello': 'world'}


def add_base_resources(api):
    """Adds base resources to the API instance.

    Args:
        api (flask_restful.Api): Flask-RESTful API instance.
    """
    api.add_resource(Base, '/')
