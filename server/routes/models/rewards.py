"""Classes, methods, and variables to support Rewards.
"""

from flask_restful import Resource

from server.database.models import rewards as db_rewards

class Rewards(Resource):
    """Route model for Rewards.
    """
    def get(self, account_id):
        """GET on /rewards/<string:account_id>.
        """
        return db_rewards.Rewards().find_by_id(int(account_id))


def add_rewards_resources(api):
    """Adds rewards resources to the API instance.

    Args:
        api (flask_restful.Api): Flask-RESTful API instance.
    """
    api.add_resource(Rewards, '/v1/rewards/<string:account_id>')
