"""Classes, methods, and variables to support TotalRewardsRemaining.
"""

from flask_restful import Resource

from server.database.models import total_rewards_remaining as db_total_rewards_remaining

class TotalRewardsRemaining(Resource):
    """Route model for TotalRewardsRemaining.
    """
    def get(self, account_id):
        """GET on /total_rewards_remaining/<string:account_id>.
        """
        return db_total_rewards_remaining.TotalRewardsRemaining().find_by_id(int(account_id))


def add_total_rewards_remaining_resources(api):
    """Adds total_rewards_remaining resources to the API instance.

    Args:
        api (flask_restful.Api): Flask-RESTful API instance.
    """
    api.add_resource(TotalRewardsRemaining, '/v1/total_rewards_remaining/<string:account_id>')
