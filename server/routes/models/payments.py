"""Classes, methods, and variables to support Payments.
"""

from flask_restful import Resource

from server.database.models import payments as db_payments

class Payments(Resource):
    """Route model for Payments.
    """
    def get(self, account_id):
        """GET on /payments/<string:account_id>.
        """
        return db_payments.Payments().find_by_id(int(account_id))


def add_payments_resources(api):
    """Adds payments resources to the API instance.

    Args:
        api (flask_restful.Api): Flask-RESTful API instance.
    """
    api.add_resource(Payments, '/v1/payments/<string:account_id>')
