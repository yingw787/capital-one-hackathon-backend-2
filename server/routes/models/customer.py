"""Classes, methods, and variables to support Customer.
"""

from flask_restful import Resource

from server.database.models import customer as db_customer

class Customer(Resource):
    """Route model for a Customer.
    """
    def get(self, customer_id):
        """GET on /customer/<string:customer_id>.
        """
        return db_customer.Customer().find_by_customer_id(int(customer_id))

class CustomersByAccount(Resource):
    """Route model for all Customers by a given Account.
    """
    def get(self, account_id):
        """GET on /customer/account/<string:account_id>.
        """
        return db_customer.Customer().find_all_by_account_id(int(account_id))


def add_customer_resources(api):
    """Adds customer resources to the API instance.

    Args:
        api (flask_restful.Api): Flask-RESTful API instance.
    """
    api.add_resource(Customer, '/v1/customer/<string:customer_id>')
    api.add_resource(CustomersByAccount, '/v1/customer/account/<string:account_id>')
