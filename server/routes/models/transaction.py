"""Classes, methods, and variables to support Transaction.
"""

from flask_restful import Resource

from server.database.models import transaction as db_transaction

class Transaction(Resource):
    """Route model for a Transaction.
    """
    def get(self, transaction_id):
        """GET on /transaction/<string:transaction_id>.
        """
        return db_transaction.Transaction().find_by_transaction_id(int(transaction_id))

class TransactionsByCustomer(Resource):
    """Route model for all Transactions by a given Customer.
    """
    def get(self, customer_id):
        """GET on /transaction/customer/<string:customer_id>.
        """
        return db_transaction.Transaction().find_all_by_customer_id(int(customer_id))

class TransactionsByAccount(Resource):
    """Route model for all Transactions by a given Account.
    """
    def get(self, account_id):
        """GET on /transaction/account/<string:account_id>.
        """
        return db_transaction.Transaction().find_all_by_account_id(int(account_id))


def add_transaction_resources(api):
    """Adds transaction resources to the API instance.

    Args:
        api (flask_restful.Api): Flask-RESTful API instance.
    """
    api.add_resource(Transaction, '/v1/transaction/<string:transaction_id>')
    api.add_resource(TransactionsByCustomer, '/v1/transaction/customer/<string:customer_id>')
    api.add_resource(TransactionsByAccount, '/v1/transaction/account/<string:account_id>')
