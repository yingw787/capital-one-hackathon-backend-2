"""Core API file.
"""

from flask_restful import Api

from server.routes.models import account
from server.routes.models import base
from server.routes.models import customer
from server.routes.models import payments
from server.routes.models import rewards
from server.routes.models import total_rewards_remaining
from server.routes.models import transaction

def generate_V1_api_from_flask_app(app):
    """Generate and populate V1 API from the Flask app.

    Args:
        app (Flask.App): Flask application instance.

    NOTE:
        - IMPERATIVE FUNCTION!
    """
    V1 = Api(app)

    account.add_account_resources(V1)
    base.add_base_resources(V1)
    customer.add_customer_resources(V1)
    payments.add_payments_resources(V1)
    rewards.add_rewards_resources(V1)
    total_rewards_remaining.add_total_rewards_remaining_resources(V1)
    transaction.add_transaction_resources(V1)
