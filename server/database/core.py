"""Core database file.
"""

import pymongo

import config

CAPITAL_ONE_DATABASE_COLLECTION = None

def get_capital_one_collection():
    """Gets the 'au_hackathon_10_27_1' collection from the 'capital_one' database where all the information is stored.
    """
    global CAPITAL_ONE_DATABASE_COLLECTION

    if not CAPITAL_ONE_DATABASE_COLLECTION:
        client = pymongo.MongoClient(config.MONGODB_URI)
        collection = client[config.CAPITAL_ONE_DATABASE_NAME][config.CAPITAL_ONE_COLLECTION_NAME]
        CAPITAL_ONE_DATABASE_COLLECTION = collection

    return CAPITAL_ONE_DATABASE_COLLECTION
