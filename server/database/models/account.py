"""Classes, methods, and variables for Account.
"""

from server.database import core as db_core

class Account(object):
    """Database model for Account.
    """
    def find_by_id(self, account_id):
        """Find an account by ID.

        Args:
            account_id (int): ID of the Account.
        """
        database_result = db_core.get_capital_one_collection().find_one({
            'account_id': account_id
        })

        database_result.pop('_id') # Prevents JSON serialization.

        # Remove fields that should be returned in a separate API endpoint.
        if 'customer' in database_result:
            database_result.pop('customer')
        if 'rewards' in database_result:
            database_result.pop('rewards')
        if 'total_rewards_remaining' in database_result:
            database_result.pop('total_rewards_remaining')
        if 'payments' in database_result:
            database_result.pop('payments')

        return database_result
