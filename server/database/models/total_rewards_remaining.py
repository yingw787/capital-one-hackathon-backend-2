"""Classes, methods, and variables for TotalRewardsRemaining.
"""

from server.database import core as db_core

class TotalRewardsRemaining(object):
    """Database model for TotalRewardsRemaining.
    """
    def find_by_id(self, account_id):
        """Find total_rewards_remaining by account ID.

        Args:
            account_id (int): ID of the account with total_rewards_remaining.
        """
        database_result = db_core.get_capital_one_collection().find_one({
            'account_id': account_id
        })

        if (database_result is None):
            raise Exception('database_result does not contain desired account.')

        if 'total_rewards_remaining' in database_result:
            return database_result['total_rewards_remaining']
        else:
            raise Exception('No total_rewards_remaining key in database_result!')
