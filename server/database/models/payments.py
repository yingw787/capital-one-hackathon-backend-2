"""Classes, methods, and variables for Payments.
"""

from server.database import core as db_core

class Payments(object):
    """Database model for Payments.
    """
    def find_by_id(self, account_id):
        """Find a transaction by account ID.

        Args:
            account_id (int): ID of the account with payments.
        """
        database_result = db_core.get_capital_one_collection().find_one({
            'account_id': account_id
        })

        if (database_result is None):
            raise Exception('database_result does not contain desired account.')

        if 'payments' in database_result:
            return database_result['payments']
        else:
            raise Exception('No payments key in database_result!')
