"""Classes, methods, and variables for Transaction.
"""

from server.database import core as db_core

class Transaction(object):
    """Database model for Transaction.
    """
    def find_by_transaction_id(self, transaction_id):
        """Find a transaction by ID.

        Args:
            transaction_id (int): ID of the transaction.
        """
        database_result = db_core.get_capital_one_collection().find_one({
            'customer.transactions.transaction_id': transaction_id
        })

        if (database_result is None):
            raise Exception('database_result does not contain desired transaction.')

        transaction = None

        for customer in database_result['customer']:
            for current_transaction in customer['transactions']:
                if current_transaction['transaction_id'] == transaction_id:
                    transaction = current_transaction
                    return transaction

        raise Exception('transaction not present.')

    def find_all_by_customer_id(self, customer_id):
        """Find all transactions by customer ID.

        Args:
            customer_id (int): ID of the customer.
        """
        database_result = db_core.get_capital_one_collection().find_one({
            'customer.customer_id': customer_id
        })

        if (
            database_result is None or
            'customer' not in database_result or
            len(database_result['customer']) == 0
        ):
            raise Exception('database_result does not contain desired customer.')

        customer = [
            customer
            for customer
            in database_result['customer']
            if customer['customer_id'] == customer_id
        ][0]

        if 'transactions' in customer:
            return customer['transactions']
        else:
            raise Exception('customer does not have any transactions!')

    def find_all_by_account_id(self, account_id):
        """Find all transactions by account ID.

        Args:
            account_id (int): ID of the account.
        """
        database_result = db_core.get_capital_one_collection().find_one({
            'account_id': account_id
        })

        transactions = []

        for customer in database_result['customer']:
            transactions += customer['transactions']

        return transactions
