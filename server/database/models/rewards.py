"""Classes, methods, and variables for Rewards.
"""

from server.database import core as db_core

class Rewards(object):
    """Database model for Rewards.
    """
    def find_by_id(self, account_id):
        """Find rewards by account ID.

        Args:
            account_id (int): ID of the account with rewards.
        """
        database_result = db_core.get_capital_one_collection().find_one({
            'account_id': account_id
        })

        if (database_result is None):
            raise Exception('database_result does not contain desired account.')

        if 'rewards' in database_result:
            return database_result['rewards']
        else:
            raise Exception('No rewards key in database_result!')
