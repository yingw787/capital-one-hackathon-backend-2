"""Classes, methods, and variables for Customer.
"""

from server.database import core as db_core

class Customer(object):
    """Database model for Customer.
    """
    def find_by_customer_id(self, customer_id):
        """Find a customer by ID.

        Args:
            customer_id (int): ID of the customer.
        """
        database_result = db_core.get_capital_one_collection().find_one({
            'customer.customer_id': customer_id
        })

        if (
            database_result is None or
            'customer' not in database_result or
            len(database_result['customer']) == 0
        ):
            raise Exception('database_result does not contain desired customer.')

        customer = [
            customer
            for customer
            in database_result['customer']
            if customer['customer_id'] == customer_id
        ][0]

        if 'transactions' in customer:
            customer.pop('transactions')

        return customer

    def find_all_by_account_id(self, account_id):
        """Find all customers for an account by account ID.

        Args:
            account_id (int): ID of the account.
        """
        database_result = db_core.get_capital_one_collection().find_one({
            'account_id': account_id
        })

        customers = database_result['customer']

        for customer in customers:
            customer.pop('transactions')

        return customers
