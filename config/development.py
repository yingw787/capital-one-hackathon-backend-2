"""Development-specific configuration variables for application.
"""

MONGODB_URI = None
CAPITAL_ONE_DATABASE_NAME = 'capital_one_heroku'
CAPITAL_ONE_COLLECTION_NAME = 'au_hackathon_final_10_27_1'
