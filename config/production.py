"""Production-specific configuration variables for application.
"""

import os

MONGODB_URI = os.getenv('MONGODB_URI')
CAPITAL_ONE_DATABASE_NAME = 'heroku_lr769qdp'
CAPITAL_ONE_COLLECTION_NAME = 'au_hackathon_final_10_27_1'
