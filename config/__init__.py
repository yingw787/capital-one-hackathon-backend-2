"""Configuration, configured based on environment.
"""

import os

environment = os.getenv('CAPITAL_ONE_HACKATHON_APPLICATION_ENVIRONMENT', 'DEVELOPMENT')

if environment == 'PRODUCTION':
    from config.production import *
elif environment == 'DEVELOPMENT':
    from config.development import *
else:
    from config.development import *
