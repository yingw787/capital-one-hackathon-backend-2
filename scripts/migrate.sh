#!/bin/sh

# Copy over files to git hooks.
cp scripts/pre-push.sh .git/hooks/pre-push

# Add permissions.
chmod +x .git/hooks/pre-push
